<?php


class YandexAnalyzer extends SomeAnalyzer
{
    // настройки
    public $resultsLimit = 50; // лимит результатов выдачи
    public $url;
    public $keyword;
    public $resultsOnPage = 50; // можно только 10, 20, 30, 50

    // функция анализа
    /**
     * @var array
     */
    public $urls;

    public function analyzeThis($url, $keyword = '')
    {
        $this->url = $url;
        $this->keyword = $keyword;
        $x = 0;
        while ($x * $this->resultsOnPage <= $this->resultsLimit - 1) {
            if ($results = $this->analyzePage(str_replace(array("\r", "\n", "\t"), '', $this->downloadPage($x)))) {
                $results[0] = $x * $this->resultsOnPage + $results[0];
                return $results;
            }
            $x++;
            sleep(rand(3, 5));
        }
        return false;
    }

    public function getAnswers($keyword = '')
    {
        $this->keyword = $keyword;
        $x = 0;
        while ($x * $this->resultsOnPage <= $this->resultsLimit - 1) {
            if ($results = $this->getPage(str_replace(array("\r", "\n", "\t"), '', $this->downloadPage($x)))) {
                $results[0] = $x * $this->resultsOnPage + $results[0];
                return $results;
            }
            $x++;
            sleep(rand(3, 5));
        }
        return false;
    }

    //// РЕАЛИЗАЦИЯ

    protected $regexpParseResults = '#<li>.*<a[^>]*tabindex[^>]*onclick[^>]*=[^>]*"[^>]*"[^>]*href="([^<>"]+)"[^>]*>(.+)</a>.*</li>#Ui';
    protected $urlMask = 'http://yandex.ru/yandsearch?text=[KEYWORD]&p=[PAGE_NUMBER]&numdoc=[RESULTS_ON_PAGE]';

    protected function downloadPage($pageNumber)
    {
        $mask = str_replace('[KEYWORD]', urlencode($this->keyword), $this->urlMask);
        $mask = str_replace('[PAGE_NUMBER]', $pageNumber, $mask);
        $mask = str_replace('[RESULTS_ON_PAGE]', $this->resultsOnPage, $mask);

        return $this->httpRequest($mask);
    }

    protected function analyzePage($content)
    {

        if (preg_match_all($this->regexpParseResults, $content, $matches, PREG_SET_ORDER) !== false) {
            if (count($matches) <= 0) {
                var_dump('<br /><span style="color: red;">Не найдено вхождений или ошибка парсера: возможно гугл подозревает, что Вы робот!</span>');
            } else {
                foreach ($matches as $num => $match) {
                    if ($this->compareURL($match[1], $this->url))
                        return array($num + 1, $match[1], $match[2]);
                }
            }
        } else var_dump('<span style="color: red;">Не найдено вхождений или ошибка парсера: возможно йандекс подозревает, что Вы робот!</span>');

        return false;
    }

    protected function getPage($content)
    {

        if (preg_match_all($this->regexpParseResults, $content, $matches, PREG_SET_ORDER) !== false) {
            if (count($matches) <= 0) {
                var_dump('<br /><span style="color: red;">Не найдено вхождений или ошибка парсера: возможно гугл подозревает, что Вы робот!</span>');
            } else {
                foreach ($matches as $num => $match) {
                    $this->urls[] = $match[1];
                }
            }
        } else var_dump('<span style="color: red;">Не найдено вхождений или ошибка парсера: возможно йандекс подозревает, что Вы робот!</span>');

        return false;
    }
}