<?php

defined('MAX_FILE_SIZE') || define('MAX_FILE_SIZE', 600000);

abstract class SomeAnalyzer
{

    // функция анализа
    public abstract function analyzeThis($url);

    // получение имени хоста из url (parse_url с дополнительным функционалом, поскольку убедился что просто parse_url не всегда почему-то работает, когда url слишком неудобочитаемый)
    public function getHost($url)
    {
        $url = @parse_url($url);
        if ($url['path'] && !$url['host'])
            $url['host'] = $url['path'];
        $url['host'] = ereg_replace("/.*$", "", $url['host']);
        $url['host'] = ereg_replace("^www\.", "", $url['host']);

        return $url['host'];
    }

    // функция сравнения 2-х url на предмет принадлежности к одному хосту
    protected function compareURL($url1, $url2)
    {
        $url1 = $this->getHost($url1);
        $url2 = $this->getHost($url2);

        return (strtoupper($url1['host']) == strtoupper($url2['host']) ? true : false);
    }

    protected function __httpRequest($url)
    {
        $headers = array(
            'cache-control: max-age=0',
            'upgrade-insecure-requests: 1',
            'user-agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
            'sec-fetch-user: ?1',
            'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'x-compress: null',
            'sec-fetch-site: none',
            'sec-fetch-mode: navigate',
            'accept-encoding: deflate, br',
            'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $html = curl_exec($ch);
        curl_close($ch);

        return $html;
    }

    protected function httpRequest(
        $url,
        $use_include_path = false,
        $context = null,
        $offset = 0,
        $maxLen = -1)
    {
        if ($maxLen <= 0) {
            $maxLen = MAX_FILE_SIZE;
        }

        return file_get_contents(
            $url,
            $use_include_path,
            $context,
            $offset,
            $maxLen
        );
    }
}